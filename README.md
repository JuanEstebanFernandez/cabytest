# SRE Architecture Test

### Solution Approach

At first sight the main problem of the actual infra is that´s built on an old-school IaaS style not taking advantages of the Cloud computing benefits, what I think could be a better solution is to have a native Cloud components such as App gateway, Kubernetes, PaaS DBs, etc, in order to get rid the bottleneck leveraging mainly on automatic vertical/horizontal escalation.

### Proposed architecture diagram

```mermaid
graph TD
subgraph "Mobile App"
Drivers["Driver"]
end
subgraph "Branch Office"
Agents["Customer Service agents"]
IntFW["Internal FW"]
end
subgraph "Cloud Vnet"
subgraph "App Gateway"
AppGW["GW"]
end
subgraph " "
CloudFW["Cloud Firewall"]
end
subgraph "Cloud Storage"
FS["NFS volume"]
end
subgraph "API Management"
PUB[Public API]
INT[Internal API]
subgraph "Kubernetes"
MS1["MicroService1"]
MS2["MicroService2"]
end
end
Agents-- "browse and administer documents through Internal API" -->IntFW 
IntFW-- "Express Route" -->AppGW
Drivers-- "browse and upload documents through Public API" -->CloudFW
CloudFW-- " " -->AppGW
AppGW-- "Distribute"-->PUB
AppGW-- "Distribute"-->INT
INT-->MS1
PUB-->MS2
MS1-- "remotely mounted from" -->FS
MS2-- "remotely mounted from" -->FS
end
subgraph "DevSecOps Team"
DSOT["CDCI"]
DSOT-->MS1
DSOT-->MS2
end
```

### Brief Explanation

¿Why two APIs?
 - having this separation permit us to address development, security and monitoring in a more dedicated way, and thinking about in cities where Cabify has a large number of internal users can be configured an Express Route to the internal API to maximize performance lowering latency.

¿Why API Manager?
To distribute automatically each request to the proper internal API and also to provide features that are useful for managing a public-facing API, including rate limiting, IP restrictions, and authentication using identity providers. 
On the other hand API Management doesn't perform any load balancing so it should be used in conjunction with a load balancer (see "Why Kubernetes?").

Why MicroServices?
Microservices are autonomous individual parts of an Application each service is a separate codebase self contained which can be managed by a small development team and should implement a single business capability in charge of improve small pieces of the solution on a daily basis because them can be deployed independently, so a team can update an existing service without rebuild and redeploy the entire application, this is also usefull to have a quick rollback in case of an outage on Production environment, and another Pros is it supports polyglot programming which means you can use the languege you need in each interconnecting them by an API.

Why Kubernetes?
This component is responsible for placing the Micro-services on nodes, identifying failures, rebalancing services across nodes, etc

***Why a PaaS DB***
Because automatic escalability, whatever horizon or vertical it be, will help us to offer to maintain a good User Experience no matter what the workload it be.

Why Cloud Storage? 
Centralizing on Cloud Storage give us the chance of get rid of OS typical issues like File Systems getting full or not performing as fast as we need, and thing about Disaster Recovery it could have a second plane copy on another geographic location.

### Action Plan
It is hard to make a detailed Action Plna withou any knowledge of the App but I´ll throw some key points:

 - I would separate in two teams, one mantaining the actual solution app & running without implementing major changes, and the other team  adapting the App for the new scenario    bearing in mind the list of known issues or problems gathered all this years.

  - Before start the infra deploy I´ll need to check with the Security Team the baseline, and if they are capable of Scan and PenTest this new infrastructure looking to find Cross-site scripting, SQL injection, Command injection, etc.

  - If possible I would like to migrate one country/Business unit at a time starting from the littlest one and learning about the roadblock or easter eggs we can find and apply the continuous improvement for the next Migration wave.

 - To have the realest test I would do a full dump of the DB and put it on the new infra (maybe we will have to anonimyze some personal data if we need to keep GDPR compliance) and in the cutover date just a differential copy will be needed.

 - As Testing is a critical stage we will have to define Quality Gates, functional testing and some group of internal user for the Canary testing stage.

 - Having enough time would allow us to perform some Chaos Engineering over the new infra in order to test his HA and Resiliency.

(last two points will give useful data to the Monitoring/Observability Team about what they have to monitor and alert in a timely manner)


Notes:

Depending on which Cloud provider Cabify relies his assets the products or technologies can differ:
Azure/Google/AWS
API layer= API Management/**/***
Kubernetes layer= AKS/GKS/EKS
Storage layer= Blob/FireBase/S3
DevSecOps layer= Azure Devops/**/***
*******App Engine Firebase?
Security Tools: Micro Focus Fortify WebInspect, SonarQube, OWASP ZAP.



